import click
import time
import json

def train_config(config, trainingfile, validationfile):
    click.secho('>>>> training with config\t {} \t {}\t {}'.format(
        config,
        trainingfile,
        validationfile)
    )
    cfgdata = open(config).read()
    tradata = open(trainingfile).read()
    valdata = open(validationfile).read()

    click.secho('config: {}'.format(cfgdata))
    click.secho('traini: {}'.format(tradata))
    click.secho('valida: {}'.format(valdata))

    click.secho('doing extremely hard work')
    NEPOCHS = 10
    for i in range(NEPOCHS):
        click.secho('Epoch {}/{}'.format(i+1,10))
        time.sleep(1)
    return {'performaces': {'train_loss': 0.999, 'val_loss': 0.987}, 'config': config}

@click.command()
@click.option('--configs')
@click.option('--trainingfile')
@click.option('--validationfile')
@click.option('--outputfile')
def train(configs,trainingfile,validationfile,outputfile):
    configs = configs.split(',')
    results = []
    for c in configs:
        result = train_config(c, trainingfile, validationfile)
        results.append(result)
    with open(outputfile,'w') as outfile:
        json.dump(results,outfile)

if __name__ == '__main__':
    train()
