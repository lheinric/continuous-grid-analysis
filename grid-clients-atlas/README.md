# ATLAS Grid Clients

(significantly based on https://github.com/vingar/rucio-cart)


```
docker build -t rucio-atlas .
docker run --rm -it -v $HOME/.globus:/root/.globus rucio-atlas bash
$> source /usr/etc/panda/panda_setup.sh
$> prun --containerImage docker://lukasheinrich/foralessandra --exec "/code/run_analysis.sh 404958 recast_sample 0.00122 /data/test01.root /data/workdir 30.0" --outDS user.lheinric.singutesttest106 --outputs hist-sample.root --site ANALY_MANC_TEST_SL7 --noBuild  --inDS user.aforti:lukastest --forceStaged --tmpDir /tmp
```
