import yaml
import os
import hashlib
import random
import subprocess
import requests
import click
import copy
import urllib

def unique_slug():
    hasher = hashlib.sha1(str(random.randint(0,100000)))
    uniq_slug = hasher.hexdigest()[:8]
    return uniq_slug

def make_state(name):
    return ATLASDDMState(name)

class ATLASDDMState(object):
    def __init__(self, name = 'analysis'):
        self.ds_name = name
        self.ds_namespace = 'user.lheinric'
        self.ds_tag = unique_slug()
    def contextualize(self, pars):
        return {
            k:v.format(
                ds_namespace = self.ds_namespace,
                ds_tag = self.ds_tag,
                **pars
            )
        for k,v in pars.items()}

def make_job(jobspec, parameters, state):
    cp = state.contextualize(parameters)
    outputs = {
        'base': jobspec['process']['outputs'].format(outDS = cp['outDS']),
        'publish': jobspec['publisher']['publish']
    }
    return {
        'image': jobspec['environment']['image'],
        'exec_string':jobspec['process']['exec_string'].format(**cp),
        'outputs': outputs,
        'inputs': jobspec['process']['inputs'].format(inDS = cp['inDS']) if 'inputs' in jobspec['process'] else None
    }

def submit(job):
    site = 'ANALY_MANC_TEST_SL7'
    cmd = [
    'prun', 
    '--containerImage', 'docker://{}'.format(job['image']),
    '--exec', "sh -c '{}'".format(job['exec_string']),
    '--outDS', job['outputs']['base'],
    '--outputs', ','.join(':'.join(x) for x in job['outputs']['publish'].items()),
    '--site',site,
    '--noBuild',
    '--forceStaged',
    '--tmpDir','/tmp',
    ]
    if job['inputs']:
        cmd += ['--inDS', job['inputs']]
    p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    out, err = p.communicate()
    taskid = int(err.splitlines()[-1].split('=')[-1])
    print('submitted task: {}'.format(taskid))

@click.group()
def grid():
    pass

@grid.command()
@click.argument('taskid')
def status(taskid):
    p = urllib.urlencode({
        'jeditaskid': taskid,
        'datasets': True,
        'json': 1,
        'username': ''
    })
    r = requests.get('http://bigpanda.cern.ch/tasks/?'+p).json()
    assert len(r) == 1
    status = r[0]['status']
    print(status)

@grid.command()
@click.argument('testname')
@click.argument('jobsfile')
def run(testname, jobsfile):
    jobspec = yaml.load(open(jobsfile))[testname]['spec']
    parameters = yaml.load(open(jobsfile))[testname]['pars']
    state = make_state(testname)

    j = make_job(jobspec, parameters, state)
    submit(j)


if __name__ == '__main__':
    grid()

