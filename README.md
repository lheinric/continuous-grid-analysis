# continuous-grid-analysis



# Locally
```
singularity exec -B $PWD/data:/data \
docker://gitlab-registry.cern.ch/lheinric/continuous-grid-analysis/helloworld_atlasbase \
sh -c '/code/analysis /data/output.txt helloworld'
```

```
docker run --rm -it -v $PWD/data:/data \
gitlab-registry.cern.ch/lheinric/continuous-grid-analysis/helloworld_atlasbase \
sh -c '/code/analysis /data/output.txt helloworld'
```

# Via `prun` 
# For now the site has to be selected
```
prun \
--containerImage docker://gitlab-registry.cern.ch/lheinric/continuous-grid-analysis/helloworld_atlasbase \
--exec "sh -c '/code/analysis /data/output.txt helloworld' \
--outDS <output_ds> --outputs output.txt --site ANALY_MANC_TEST_SL7 --noBuild
```

